NAME
####

::

 RSSBOT - feed rss into your channel


DESCRIPTION
===========

::

 RSSBOT is a python3 bot able to display rss feeds in your channel.
 It provides all the tools to program a bot, such as disk perisistence
 for configuration files, event handler to handle the client/server
 connection, code to introspect modules for commands, deferred
 exception handling to not crash on an error, a parser to parse
 commandline options and values, etc.

 You can also copy/paste the service file and run it under systemd for
 24/7 presence in a IRC channel.

 RSSBOT is a contribution back to society and is Public Domain.


SYNOPSIS
========

::

 rssbot <cmd> [key=val] 
 rssbot <cmd> [key==val]
 rssbot [-c] [-d] [-v] [-i]


INSTALL
=======

::

 pipx install rssbot


USAGE
=====


default action is doing nothing::

 $ rssbot
 $

first argument is a command::

 $ rssbot cmd
 cfg,cmd,dlt,dne,dpl,fnd,log,met,mod,mre,
 nme,pwd,rem,rss,sts,tdo,thr,ver

starting a console requires an option::

 $ rssbot -c
 >

list of modules::

 $ rssbot mod
 bsc,err,flt,irc,log,mod,rss,shp,sts,tdo,
 thr,udp

to start the prg as daemon::

 $ rssbot -d
 $ 

add -v if you want to have verbose logging::

 $ rssbot -cv
 PRG started Wed Nov 8 15:38:56 2023 CVI
 >


CONFIGURATION
=============


irc configuration is done with the cli interface
using the ``cfg`` command::

 $ rssbot cfg server=<server>
 $ rssbot cfg channel=<channel>
 $ rssbot cfg nick=<nick>

sasl need a nickserv nick/password pair to generate
a password for sasl::

 $ rssbot pwd <nsnick> <nspass>
 $ rssbot cfg password=<frompwd>

rss has several configuration commands::

 $ rssbot rss <url>
 $ rssbot dpl <url> <item1,item2>
 $ rssbot rem <url>
 $ rssbot nme <url> <name>


COMMANDS
========

here is a list of the most basic commands::

 cfg - irc configuration
 cmd - commands
 dlt - remove a user
 dne - mark todo as done
 dpl - sets display items
 fnd - find objects 
 log - log some text
 met - add a user
 mre - displays cached output
 nme - display name of a feed
 pwd - sasl nickserv name/pass
 rem - removes a rss feed
 rss - add a feed
 sts - show status
 tdo - add todo item
 thr - show the running threads


SYSTEMD
=======

save the following it in /etc/systems/system/rssbot.service and
replace "<user>" with the user running pipx::

 [Unit]
 Description=feeding rss into your channel
 Requires=network.target
 After=network.target

 [Service]
 Type=simple
 User=<user>
 Group=<user>
 WorkingDirectory=/home/<user>/.rssbot
 ExecStart=/home/<user>/.local/pipx/venvs/libprg/bin/rssbot -d
 RemainAfterExit=yes

 [Install]
 WantedBy=multi-user.target

then run this::

 sudo systemctl enable rssbot --now

 default channel/server is #rssbot on localhost


FILES
=====

::

 ~/.rsbot
 ~/.local/bin/rssbot
 ~/.local/pipx/venvs/rssbot/


AUTHOR
======

::

 Bart Thate <bthate@dds.nl>


COPYRIGHT
=========

::

 RSSBOT is a contribution back to society and is Public Domain.
